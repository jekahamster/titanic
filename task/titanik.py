import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    groups = [', Mr.', ', Mrs.', ', Miss.']
    
    dfs = []
    for group in groups:
        df_ = df[df['Name'].str.contains(group)]
        dfs.append((group, df_))

    ans = []
    for title, df_ in dfs:
        # na_count = df_['Age'].isna().sum()
        na_count = df_['Age'].isnull().sum()
        median_age = round(df_['Age'].median())
        ans.append((title, na_count, median_age))

    return ans
